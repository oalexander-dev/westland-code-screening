/** Begin Helpers * */

/** You may write any number of helper functions here ( or in other files, should you wish) * */
import { fetchWorkOrders, getContact } from './apis/fetch-wo';
import { WorkOrder } from './types/types';

/**
 * An important helper to determine if you're a talented and gifted developer!
 */
export const areYouAwesome = () : boolean => true;

/** End Helpers * */

/**
 * The function is what will be called by main.ts. The data you return here will be consumed by an
 * email service that will send each point of contact the list of work orders that have been assigned to them.
 * You can return this data in any format you think suits the above. Be sure to read the readme for further requirements.
 *
 * Free to change the type of your return!
 */
export const processWorkOrders = async () : Promise<any> => {
  const workOrders: WorkOrder[] = await fetchWorkOrders();
  const pointOfContact: string = getContact('Plumbing', 0);

  return [];
};
